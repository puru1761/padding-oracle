# Padding Oracle Attack on AES-CBC

This repository contains code for implementing a padding oracle attack against a vulnerable decryptor to decrypt a file encrypted by the AES-128 CBC algorithm and an unknown secret key. This attack requires no knowledge of the key and works based on the error messages the decryptor throws.

## Build and Run Instructions

In order to run this code, clone this repository and build the decryptor and attacker independently as follows:

##### Attacker:
```
$ cd padding-oracle
$ go build decrypt-attack.go
```
##### Decryptor:
```
$ cd decrypt-test
$ go build decrypt-test.go
```

### Attacker usage:
```
$ ./decrypt-attack -h
[X] PADDING ORACLE ATTACK TOOL [X]
This is a tool to execute a padding oracle attack on
a vulnerable decryptor using AES-128 CBC Mode with a
hardcoded key

usage: ./decrypt-attack -i FILE
-----------------------------------------------------------
FLAG     MEANING
-i       The encrypted file

```

According to the above usage, the way to run the attacker is:
```
$ ./decrypt-attack -i test/out.enc

[*] Decrypting Block 9
12/16 bytes retrieved
```

The path to the decryptor is hardcoded on line 142 of the decrypt-attack.go file. This path can be altered to point to the target decryptor program.

## Contact

* Author: Purushottam Kulkarni
* Email: pkulkar6@jhu.edu

