package main

import (
	"fmt"
	"io/ioutil"
	"strings"
	"os"
	"os/exec"
	"crypto/aes"
)

/*
Error Checker
*/
func check(e error) {

	if e!=nil {

		fmt.Println("\n\033[91m[!]\033[0m ERROR:", e)
		fmt.Println("\n")
		os.Exit(0)
	}
}

/*
To show the usage of our tool
*/
func show_usage() {

	fmt.Println("\033[91m[X]\033[0m PADDING ORACLE ATTACK TOOL \033[91m[X]\033[0m")
	fmt.Println("This is a tool to execute a padding oracle attack on\na vulnerable decryptor using AES-128 CBC Mode with a\nhardcoded key")
	fmt.Println("")
	fmt.Println("usage: ./decrypt-attack -i FILE")
	fmt.Println("-----------------------------------------------------------")
	fmt.Println("FLAG\t", "MEANING")
	fmt.Println("-i\t", "The encrypted file")
	fmt.Println("")
}

/*
Parsing the arguments
*/
func flagParse(args []string) string {

	if len(args) != 3 {
		show_usage()
		os.Exit(0)
	}
	if args[1] == "-i" {
		return args[2]
	} else {
		show_usage()
		os.Exit(0)
	}

	return ""
}

/*
Removing padding and extracting the MAC for our final output
*/
func remove_pad(data []byte) []byte {

	pad_len := int(data[len(data)-1])
	//fmt.Printf("\033[92m[+]\033[0m Extracted MAC: %x\n", data[len(data)-pad_len-32:len(data)-pad_len])

	return data[0:len(data)-pad_len-32]

}

/*
Reverse padding function for blocks less than 64 bytes
*/
func pad_reverse(data []byte) []byte {

	var output []byte

	n := (aes.BlockSize*4) - len(data)

	for i:=0;i<n;i++ {
		output = append(output, byte(0x00))
	}
	output = append(output, data...)

	return output
}

/*
Function to reverse the byte-array
*/
func reverse(data []byte) []byte {
	for i, j := 0, len(data)-1; i < j; i, j = i+1, j-1 {
		data[i], data[j] = data[j], data[i]
	}

	return data
}

/*
The actual attack script.
Contains 4 loops to prepare and deliver the ciphertext
to the actual decryptor.
Makes use of the actual padding oracle attack algorithm
*/
func oracle_attack(ciphertext []byte, file string) []byte {

	var recovered_bytes []byte
	var pt []byte
	var data []byte

	num_blocks := (len(ciphertext)/aes.BlockSize) - 1

	fmt.Println("")
	for blk:=0;blk<num_blocks;blk++ {
		//fmt.Println("\033[94m[*]\033[0m Decrypting Block", num_blocks-blk)
		for i:=1;i<=aes.BlockSize;i++ {

			for j:=1;j<256;j++ {

				data = append(data, ciphertext...)
				data = data[0:len(data)-aes.BlockSize*blk]

				if len(data) == len(ciphertext) && (i==1 && j==1) {
					j = j +1
				}

				if len(data) < aes.BlockSize*4 {
					data = pad_reverse(data)
				}

				for k:=1;k<=i;k++ {
					if k==i {
						data[len(data)-aes.BlockSize-k] = data[len(data)-aes.BlockSize-k] ^ byte(i) ^ byte(j)
					} else {
						data[len(data)-aes.BlockSize-k] = data[len(data)-aes.BlockSize-k] ^ byte(i) ^ recovered_bytes[k-1]
					}
				}

				e := ioutil.WriteFile("tmp", data, 0666)
				check(e)

				result, err := exec.Command("decrypt-test/decrypt-test", "-i", "tmp").Output()
				check(err)

				if strings.Compare(string(result), "INVALID PADDING") != 0 {

					recovered_bytes = append(recovered_bytes, byte(j))
					break

				}
			}
			data = data[:0]

			/*if i==16 {
				fmt.Printf("\033[92m")
			}
			fmt.Printf("\r%d/16 bytes retrieved", i)
			if i==16 {
				fmt.Printf("\033[0m")
			}*/

		}
		//fmt.Println("\n")
		pt = append(pt, recovered_bytes...)
		recovered_bytes = recovered_bytes[:0]
	}
	os.Remove("tmp")

	reverse(pt)

	pt = remove_pad(pt)

	return pt
}

func main() {

	file := flagParse(os.Args)
	data, err := ioutil.ReadFile(file)
	check(err)

	decrypted_pt := oracle_attack(data, file)
	if len(decrypted_pt) == 0 {
		fmt.Println("\033[91m[X]\033[0m Failed to find padding length.")
		os.Exit(0)
	}
	//fmt.Println("\033[92m[+]\033[0m File Decrypted and contains:\n", string(decrypted_pt))
	fmt.Println(string(decrypted_pt))
}
